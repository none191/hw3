# Домашнее задание №3

## Требования

Алгоритмы должны быть асимптотически оптимальными по времени.

Нельзя пользоваться частичными функциями (head, tail).

## Тесты

Ко всем функциям обязательно писать тесты: кейсы или проперти. 1 - 2 достаточно. 

Тесты свойств (property) — это тесты со случайно выбираемыми параметрами.

```hs

-- кейсы — с фиксированными параметрами
prop_example1 :: Property
prop_example1 xs = head (sort [3, 4, 1]) === minimum [3, 4, 1]

-- проперти — со случайно выбираемыми параметрами
prop_example1 :: [A] -> Property
prop_example1 xs = head (sort xs) === minimum xs

prop_example1 :: NonEmpty A -> Bool
prop_example1 xs = not $ null xs
```

Тесты для инстансов можно писать на основе законов классов:

```hs
prop_Functor_Identity :: f A -> Property
prop_Functor_Identity x =
  fmap id x === x

prop_Functor_Composition :: Fun B C -> Fun A B -> f A -> Property
prop_Functor_Composition (Fun _ f) (Fun _ g) x =
  fmap (f . g) x === (fmap f . fmap g) x

prop_Applicative_Identity :: f A -> Property
prop_Applicative_Identity v =
  (pure id <*> v) === v

prop_Applicative_Composition :: f (Fun B C) -> f (Fun A B) -> f A -> Property
prop_Applicative_Composition u' v' w =
  (pure (.) <*> u <*> v <*> w) === (u <*> (v <*> w))
  where
    u = applyFun <$> u'
    v = applyFun <$> v'

prop_Applicative_Homomorphism :: Fun A B -> A -> Property
prop_Applicative_Homomorphism (Fun _ f) x =
  (pure f <*> pure x) === (pure (f x) :: f B)

prop_Applicative_Interchange :: f (Fun A B) -> A -> Property
prop_Applicative_Interchange u' y =
  (u <*> pure y) === (pure ($ y) <*> u)
  where
    u = applyFun <$> u'

prop_Monad_LeftIdentity :: A -> Fun A (m B) -> Property
prop_Monad_LeftIdentity a (Fun _ k) =
  (return a >>= k) === k a

prop_Monad_RightIdentity :: m B -> Property
prop_Monad_RightIdentity m =
  (m >>= return) === m

prop_Monad_Associativity :: f A -> Fun A (f B) -> Fun B (f C) -> Property
prop_Monad_Associativity m (Fun _ k) (Fun _ h) =
  (m >>= (\x -> k x >>= h)) === ((m >>= k) >>= h)

prop_traverse_Identity :: t A -> Property
prop_traverse_Identity x =
  traverse Identity x === Identity x

prop_traverse_Composition :: Fun A (F B) -> Fun B (G C) -> t A -> Property
prop_traverse_Composition (Fun _ f) (Fun _ g) x =
  traverse (Compose . fmap g . f) x
    === (Compose . fmap (traverse g) . traverse f) x

prop_sequenceA_Identity :: t A -> Property
prop_sequenceA_Identity x =
  (sequenceA . fmap Identity) x === Identity x

prop_sequenceA_Composition :: t (F (G A)) -> Property
prop_sequenceA_Composition x =
  (sequenceA . fmap Compose) x === (Compose . fmap sequenceA . sequenceA) x

type F = Maybe

type G = Either String
```

## 1. Simple Moving Average

Реализуйте [Simple Moving Average](https://en.wikipedia.org/wiki/Moving_average) алгоритм, используя State. Надо придумать, как реализовать алгоритм с изменяемым значением, определив, какая часть данных должна изменяться и передаваться между итерациями.

```hs
ghci> moving 4 [1, 5, 3, 8, 7, 9, 6]
[1.0, 3.0, 3.0, 4.25, 5.75, 6.75, 7.5]

ghci> moving 2 [1, 5, 3, 8, 7, 9, 6]
[1.0, 3.0, 4.0, 5.5, 7.5, 8.0, 7.5]
```

## RWS = Reader + Writer + State

### 2-4. Инстансы для RWS

```hs
newtype RWS r w s a = RWS {runRWS :: r -> s -> (a, s, w)}
```

RWS имеет семантику, аналогичную Reader по параметру _r_, Writer по _w_ (который может быть моноидом), State по _s_.

Реализовать истансы без deriving:

2. Functor
3. Applicative
4. Monad

### 5-7. Полезные ридеры

```hs
-- | 5. Построение простого ридера
reader :: (r -> a) -> RWS r w s a

-- | 6. Просмотр окружения
ask :: RWS r w s r

-- | 7. Вычисление в модифицированном окружении
local :: (r -> r) -> RWS r w s a -> RWS r w s a
```

### 8-10. Полезные райтеры

```hs
-- | 8. Построение простого райтера
writer :: (a, w) -> RWS r w s a

-- | 9. Вывод во Writer
tell :: w -> RWS r w s ()

-- | 10. Запуск временного Writer и получение результата
listen :: RWS r w s a -> RWS r w s (a, w)
```

### 11-13. Полезные State-действия

```hs
-- | 11. Построение простого State-действия
state :: (s -> (a, s)) -> RWS r w s a

-- | 12. Чтение
get :: RWS r w s s

-- | 13. Запись
put :: s -> RWS r w s ()
```
